
var digdug = digdug || {};


var localStorageName = "RANKING";
var ranking = [
    10,
    300,
    200,
    100,
    500
]

digdug.introScene={
    
    preload:function(){
        //this.load.video('intro', 'assets/video/intro.mp4');
        this.load.spritesheet('intro','assets/img/intro_SP.png',593,767);
        localStorage.setItem(localStorageName, JSON.stringify(ranking));
        this.rankinginstorage = JSON.parse(localStorage.getItem(localStorageName));
        this.rankinginstorage.sort(function(a,b){return b-a});
    },
    
    create:function() {
        /*
        this.video = this.game.add.video('intro');
        this.video.muted = true;
        this.video.autoplay = true;
        this.video.loop = false;

        this.video.addToWorld(0, 25, 0, 0,1.7,1.7);

        
        this.video.play(true);*/
        
        this.intro = this.game.add.sprite(0, 25, 'intro');
        this.intro.scale.setTo(0.79);
        this.slowMo = 5;
        this.currentSlowTime = 0;
        
    },
    update:function(){
        this.currentSlowTime++;
        if (this.currentSlowTime >= this.slowMo){
            this.intro.frame ++;
            this.currentSlowTime = 0;
        }
        
        if (this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).isDown){
            this.intro.frame = 326;
        }
        
        if (this.intro.frame >= 326){
            this.game.state.start('loadlevel', this);
        }
        /*
        if (!this.video.playing)
            this.game.state.start('loadlevel', this);*/
    }
}
