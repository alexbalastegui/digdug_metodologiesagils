var digdug = digdug || {};


var localStorageName = "RANKING";

digdug.rankingState={
    preload:function(){
        console.log(ranking.length);
        //localStorage.setItem(localStorageName, JSON.stringify(ranking));
        this.cont = 0;
        this.name = '';
        this.margin=10; 
        this.punt = 300;
        this.addnewplayer = false;
        this.load.audio('rankingtheme', 'assets/audio/rankingtheme.mp3');
        this.delay = 0;
    },
    
    keyPress:function(key){
        if(this.cont < 3){
           this.cont++;
           this.name+=key; 
        }
    },
    updateranking:function(param){
        param.splice(param.length-1,1);
        console.log(param);
        param.push(this.score);
    },
    create:function(){
        this.themesong = digdug.game.add.audio('rankingtheme');
        this.themesong.play();
        this.title=this.game.add.text(gameVars.gameWidth/2,this.game.world.centerY/2,'RANKING', {font: '50px arcade',fill: '#ff0000'});
        this.line=this.game.add.text(gameVars.gameWidth/2,this.game.world.centerY/2+20,'--------', {font: '50px arcade',fill: '#00ff00'});
        this.title.anchor.setTo(.5);
        this.line.anchor.setTo(.5);
        console.log(this.name);
        
        this.rankinginstorage = JSON.parse(localStorage.getItem(localStorageName));
        this.rankinginstorage.sort(function(a,b){return b-a});

        digdug.game.input.keyboard.addCallbacks(this, null, null, this.keyPress);
        
        this.updateranking(this.rankinginstorage);
        this.rankinginstorage.sort(function(a,b){return b-a});
        
        for(i=0;i<this.rankinginstorage.length;i++){
            this.rankingVal=this.game.add.text(gameVars.gameWidth/2-50,this.game.world.centerY/2+40+this.margin,this.rankinginstorage[i], {font: '50px arcade',fill: '#ffff00'});
            //this.rankingVal.anchor.setTo(.5);
            this.margin+=50;
        }
        localStorage.setItem(localStorageName, JSON.stringify(this.rankinginstorage));
        
    },
    update:function(){
        console.log(this.delay);
        if(this.delay == 600){
            digdug.loadlevel.resetLives();
            if (this.themesong.isPlaying){
                this.themesong.stop();
            }            
            this.game.state.start('loadlevel', this);
        }
        this.delay++;
    }
}