var digdug = digdug || {};

var gameVars={
    gameWidth:468,
    gameHeight:684,
    gravity:10,
    gameCell:36
}

digdug.game = new Phaser.Game(gameVars.gameWidth,gameVars.gameHeight,Phaser.AUTO,null,this,false,false);

digdug.game.state.add('main',digdug.gameState);
digdug.game.state.add('rankingState', digdug.rankingState);
digdug.game.state.add('loadlevel', digdug.loadlevel);
digdug.game.state.add('creditsscene', digdug.creditsscene);
digdug.game.state.add('introscene', digdug.introScene);
digdug.game.state.start('introscene');