var digdug = digdug || {};

var gridDirect = "STOP";

digdug.enemy=function(game, x, y, speed, direction, level, time, id, Path, type){
    Phaser.Sprite.call(this,game, x, y, type);
    this.animations.add('walk',[0,1],6,true);
    this.animations.add('smashed',[2,3],6,true);
    this.animations.add('attack',[0,4],6,true);
    this.animations.add('ghost',[5,6],6,true);
    this.animations.play('walk');
    this.type = type;
    this.mygame = game;
    this.myPosX = x;
    this.myPosY = y;
    this.myDirection = direction;
    this.mySpeed = speed;
    this.myLevel = level;
    this.birthTime=this.myLevel.time.now;
    this.myTime = time;
    this.anchor.setTo(.5);
    this.currentState = 0;
    this.id = id;
    this.myPath;
    this.isAlive=true;
    
    
    this.mygame.physics.arcade.enable(this, Phaser.Physics.ARCADE);
    this.body.allowGravity = false;

    this.lowerBox = false;
    this.upperBox = false;
    this.leftBox = false;
    this.rightBox = false;

    
    this.havePlayer = false;
    
    this.inflation = 0;
    this.delayedDead = false;
    this.rocked = false;
    
    this.fireDetector = null;
    
    if (this.type == 'dragon'){
        this.fireDetector = this.addChild(this.game.make.sprite(-10, 10, 'invisibleCol'));
        this.fireDetector.alpha = 0; //<-------------------------------- makes it invisible comment for debuging
        this.fireDetector.anchor.setTo(1);
        this.game.physics.arcade.enable(this.fireDetector);
    }
    this.inFireZone = false;
    this.timeInZone = 0;
    
    this.blownSound = digdug.game.add.audio('m_BlownAudio');
    this.squashSound = digdug.game.add.audio('m_SquashAudio');
    this.fireSound = digdug.game.add.audio('firingAudio');
    this.hitSound = digdug.game.add.audio('m_HitDAudio');
    this.pumpSound = digdug.game.add.audio('pumpAudio');
    this.squashSounded = false;
    this.pumpSounded = false;
    this.pumpSoundEnabled = true;
    
    this.fire = null;
           
};

digdug.enemy.prototype = Object.create(Phaser.Sprite.prototype);
digdug.enemy.prototype.constructor = digdug.enemy;


digdug.enemy.prototype.update = function(){
    console.log(this.inflation);
    this.body.collideWorldBounds=true;
    var myGridX = Math.floor((this.body.position.x + 36/2) * 25 / 468); //my position in the grid
    var myGridY = Math.floor((this.body.position.y - 72 + 36/2) * 33 / 612); //(-72 to delete unused sky pixels)
    var playerGridX = Math.floor((this.myLevel.hero.sprite.body.position.x + 36/2) * 25 / 468); //my position in the grid
    var playerGridY = Math.floor((this.myLevel.hero.sprite.body.position.y - 72 + 36/2) * 33 / 612); //(-72 to delete unused sky pixels)
    
    if (this.rocked){
        if (this.delayedDead){
            this.frame = 3;
        }else{
            if(!this.squashSound.isPlaying && !this.squashSounded){
                this.squashSound.play(); 
                this.squashSounded = true;
                this.isAlive=false;
            }
            this.frame = 2;
            this.body.velocity.x = 0;
        }
    }
    else if(this.inflation<=0)
    {
        this.pumpSounded = false;
        switch(this.currentState){
            case 0:
            default: //PATROL
                this.animations.play('walk');
                this.rightinHole = false;
                this.leftinHole = false;
                this.upperinHole = false;
                this.lowerinHole = false;
                this.counter = 0;
                this.game.physics.arcade.overlap(this, this.myLevel.hero.sprite, this.DamagePlayer, null, this);

                this.lowerBox = false;
                this.upperBox = false;
                this.leftBox = false;
                this.rightBox = false;
                
                if(this.myLevel.grid[myGridY][myGridX+1] == 1){
                    this.leftBox = true;
                }
                if(this.myLevel.grid[myGridY][myGridX-1] == 1){
                    this.rightBox = true;
                }
                if(this.myLevel.grid[myGridY+1][myGridX] == 1){
                    this.upperBox = true;
                }
                if(this.myLevel.grid[myGridY-1][myGridX] == 1){
                    this.lowerBox = true;
                }
                this.body.velocity.x = this.mySpeed*this.myDirection;
                if(this.rightBox || this.leftBox){
                    this.body.velocity.x = 0;
                   if(!this.rightBox){
                        this.myDirection = -1;
                        this.scale.x = this.myDirection * -1;
                        this.body.velocity.x = this.mySpeed*this.myDirection;
                   }
                   if(!this.leftBox){
                        this.myDirection = 1;
                        this.scale.x = this.myDirection * -1;
                        this.body.velocity.x = this.mySpeed * this.myDirection;
                   }
                }
                this.body.velocity.y = this.mySpeed*this.myDirection;
                if(this.upperBox || this.lowerBox){
                    this.body.velocity.y = 0;
                   if(!this.upperBox){
                        this.myDirection = 1;
                        this.scale.x = this.myDirection * -1;
                        this.body.velocity.y = this.mySpeed*this.myDirection;
                   }
                   if(!this.lowerBox){
                        this.myDirection = -1
                        this.scale.x = this.myDirection * -1;
                        this.body.velocity.y = this.mySpeed*this.myDirection;
                   }
                }  
                if((this.myLevel.time.now-this.birthTime) > this.myTime){
                    this.currentState = 1;
                }
                break;
            case 1: //PHANTOM MODE    
                this.animations.play('ghost');
                this.body.setSize(0, 0, 0, 0);
                if (!this.havePlayer){
                    this.havePlayer = true;
                    this.playerx = this.myLevel.hero.sprite.body.position.x + 36/2;
                    this.playery = this.myLevel.hero.sprite.body.position.y + 36/2; //This makes error not sure why
                }
                this.mygame.physics.arcade.moveToXY(this, this.playerx, this.playery, this.mySpeed, 2500); //function that makes the enemy go to that position
                var range = Math.abs(this.body.sprite.position.x - this.playerx) +  Math.abs(this.body.sprite.position.y - this.playery);
                //console.log(range);

                if (range <= 5){
                    this.havePlayer = false;
                    this.body.setSize(36, 36, 0, 0);
                    this.currentState = 2;
                }
                break;
            case 2: //FOLLOW PLAYER
                
                this.inFireZone = false;
                
                if (this.type == 'dragon'){
                    this.game.physics.arcade.overlap(this.fireDetector, this.myLevel.hero.sprite, this.fireZone, null, this);
                    if (this.fire != null && this.timeInZone >= 70){
                        this.game.physics.arcade.overlap(this.fire, this.myLevel.hero.sprite, this.DamagePlayer, null, this);
                    }
                }
                
                if (this.inFireZone){
                    this.animations.play('attack');
                    this.body.velocity.x = 0;
                    this.body.velocity.y = 0;
                    
                    this.timeInZone ++;
                    //flash for attack
                    if(this.timeInZone >= 50 && this.fire == null){
                        this.fire = this.addChild(this.game.make.sprite(-10, 20, 'fire'));
                        this.fire.animations.add('fire',[0,1,2],6, false);
                        this.fire.anchor.setTo(1);
                        this.fire.animations.play('fire');
                        this.game.physics.arcade.enable(this.fire);
                        if(!this.fireSound.isPlaying){
                            this.fireSound.play();
                        }                        
                        this.game.physics.arcade.overlap(this.fire, this.myLevel.hero.sprite, this.DamagePlayer, null, this);

                        this.fire.animations.currentAnim.onComplete.add(function () {
                            this.timeInZone = 0;
                            this.fire.destroy();
                            this.fire = null;
                        }, this);
                    }
                }else {
                    if(this.timeInZone > 0){
                        this.timeInZone--;
                    }
                    this.animations.play('walk');
                    this.game.physics.arcade.collide(this, this.myLevel.hero.sprite, this.DamagePlayer, null, this);
                    this.easystar = new EasyStar.js();
                    this.easystar.setGrid(this.myLevel.grid);
                    this.easystar.setAcceptableTiles([0]);
                    if (this.myLevel.id == this.id){
                            this.easystar.findPath(myGridX, myGridY, playerGridX, playerGridY, function( path ) {
                            if (path === null) {
                                this.currentState = 1;
                                gridDirect = "STOP";
                            } else {
                                if(path[1].x == myGridX && path[1].y == myGridY){
                                    gridDirect = "STOP";
                                }else if (path[1].x < myGridX)
                                {
                                    gridDirect = "ESQ";                    
                                }else if (path[1].x > myGridX)
                                {
                                    gridDirect = "DRT";                    
                                }else if (path[1].y < myGridY)
                                {
                                    gridDirect = "UP";                     
                                }else if (path[1].y > myGridY)
                                {
                                    gridDirect = "DWN";                   
                                }
                            }
                        });
                        switch(gridDirect){
                            case "STOP":
                                this.body.velocity.x = 0;
                                this.body.velocity.y = 0;
                                break;
                            case "DWN":
                                this.scale.x = -1;
                                //this.body.sprite.angle = 270;
                                this.body.velocity.x = 0;
                                this.body.velocity.y = 40;
                                break;
                            case "UP":
                                this.scale.x = 1;
                                //this.body.sprite.angle = 90;
                                this.body.velocity.x = 0;
                                this.body.velocity.y = -40;
                                break;
                            case "DRT":
                                this.scale.x = -1;
                                this.body.velocity.x = 40;
                                this.body.velocity.y = 0;
                                break;
                            case "ESQ":
                                this.scale.x = 1;
                                //this.body.sprite.angle = 0;
                                this.body.velocity.x = -40;
                                this.body.velocity.y = 0;
                                break;
                        }

                        this.easystar.calculate();
                    }
                }
                
                
                break;
        }
        
    }
    else //Controlar inflacion
    {
        this.body.velocity.x = 0;
        this.body.velocity.y = 0;
        
        if (this.inflation < 25){ //cambio de imagen segun inflacion
            this.scale.setTo(1.25);
            this.frame = 7;
            //console.log("Inflation lvl 1");
            if(!this.pumpSounded && this.pumpSoundEnabled){
                this.pumpSound.play();
                this.pumpSounded = true;
            }
            
        }
        else if (this.inflation < 50)
        { 
            this.scale.setTo(1.5);
            if (this.delayedDead){
                this.destroy()
            }else{
                this.frame = 8; 
                //console.log("Inflation lvl 2");
                if(!this.pumpSounded){
                    this.pumpSound.play();
                    this.pumpSounded = true;
                }
            }  
        }
        else if (this.inflation >= 50) //ESTO LO HEMOS CAMBIADO
        {
            this.blownSound.play();
            this.scale.setTo(2);
            this.isAlive=false;
            this.frame = 9; 
            this.delayedDead = true; //para mantener imagen hasta la reduccion de tamaño
        }
        if (this.inflation - 0.15 >= 0){
            this.inflation -= 0.15;
        }else{
            this.inflation = 0;
        }
    }
        
};

digdug.enemy.prototype.PhantomMode = function(x_coorObj, y_coorObj){
    
};

digdug.enemy.Die = function (){
    
};

digdug.enemy.StartMovement = function(){
    
}

digdug.enemy.prototype.RightHole = function(right,hole){
    this.rightinHole = true;
}

digdug.enemy.prototype.LeftHole = function(right,hole){
    this.leftinHole = true;
}

digdug.enemy.prototype.UpperHole = function(right,hole){
    this.upperinHole = true;
}

digdug.enemy.prototype.LowerHole = function(right,hole){
    this.lowerinHole = true;
}

digdug.enemy.prototype.DamagePlayer = function(right,hole){
    if(!this.hitSound.isPlaying){
        this.hitSound.play();
        this.myLevel.hero.kill();
        console.log("enemy do damage");
    }    
}

digdug.enemy.prototype.fireZone = function(fireZone, hero){
    this.inFireZone = true;
    console.log('In fire zone');
}

digdug.enemy.prototype.InflateMe = function(){ 
    //cuando este metido el modo conectar e inflar meter un +=50 con cada click en lugar de por tick en connexion
    //console.log("Inflate me");
    if (!this.delayedDead){
        this.inflation+=20;
        if (this.inflation >= 25 && this.pumpSoundEnabled){
            this.pumpSounded = false;
            this.pumpSoundEnabled = false;
        }
    }    
}

digdug.enemy.prototype.stopMe = function(){     
    this.body.move = false;
    this.body.velocity.y = 0;
    this.body.setSize(15, 36, 21, 0);
    console.log("STOP PLEASE, I'm beging you! ... :_(");
}

digdug.enemy.prototype.killMe = function(){
    this.isAlive = false;
}
