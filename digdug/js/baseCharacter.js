var digdug = digdug || {};

digdug.baseCharacter = {
    id:"",
    size: 1,
    posX:0,
    posY:0,
    color: 0xffffff,
    velocity:1,
    collidingList:[],
    explosiveList:[],
    canCollide:true,
    pad:0,
    hearInput:true,
    velMode:false,
    arrow:0,
    frontVec:'n',
    enemies: [],
    isAlive:true,
    canBeHarmed:true,
    timer:0,
    resetLevel:false,
    
    //phaser methods
    preload:function(){
        digdug.game.load.image('hole', 'assets/img/Hole.png');
        digdug.game.load.spritesheet('arrowRight','assets/img/heroArrowRight.png',55,7);
    },
    create:function(spawnX, spawnY, spriteName){
        this.posX=spawnX,
        this.posY=spawnY,
        this.sprite=digdug.game.add.sprite(this.posX,this.posY,spriteName,0);
        this.sprite.scale.setTo(this.size);
        this.sprite.anchor.setTo(.5);
        this.sprite.tint = this.color;      
        this.sprite.punt = 0;
        
        this.pad=digdug.game.input.keyboard.createCursorKeys();
        this.attackButton=digdug.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        
        this.attackButtonNewState=this.attackButtonLastState=true;
        this.canBeHarmed=true;
        
        
        this.walkMusic = digdug.game.add.audio('walkAudio');
        this.attackSound = digdug.game.add.audio('harpoonAudio');
    },
    update:function(){
        this.attackButtonNewState=this.attackButton.isUp;
        
        if (this.canMove){
            if (!this.walkMusic.isPlaying && this.isAlive){
                this.walkMusic.play();
             }
        }else{
            this.walkMusic.stop();
        }
        
        if (this.arrow == 0){            
            this.attackSounded = false;
        }else{
            if(!this.attackSounded){
                this.attackSound.play();
                this.attackSounded = true;
            }              
        }
        
        //movement
        if(this.hearInput==true){        
            if(this.velMode==false){
                if(this.pad.left.isDown){
                    this.canMove=true;
                    if(this.posY%gameVars.gameCell==0 || this.frontVec=='r' || this.frontVec=='n'){
                        this.frontVec='l';
                        this.animLock=false;
                    }
                }                    
                else if(this.pad.right.isDown){     
                    this.canMove=true;    
                    if(this.posY%gameVars.gameCell==0 || this.frontVec=='l' || this.frontVec=='n'){
                        this.frontVec='r';
                        this.animLock=false;
                    }
                }          
                else if(this.pad.up.isDown){    
                    this.canMove=true;       
                    if(this.posX%gameVars.gameCell==0 || this.frontVec=='d' || this.frontVec=='n'){ 
                        this.frontVec='u';
                        this.animLock=false;
                    }
                }

                else if(this.pad.down.isDown){  
                    this.canMove=true;   
                    if(this.posX%gameVars.gameCell==0 || this.frontVec=='u' || this.frontVec=='n'){      
                        this.frontVec='d';
                        this.animLock=false;
                    }
                }
                
                if(this.pad.up.isUp && this.pad.down.isUp && this.pad.left.isUp && this.pad.right.isUp){
                    this.canMove=false;
                    this.animLock=true;
                    this.stopAnimation();
                }
                
                if(this.canMove){
                    switch(this.frontVec){
                        case'l': 
                            this.sprite.body.position.x-=this.velocity;
                            this.sprite.scale.x=-this.size;
                            this.playAnimation('runSide');
                            break;
                        case'r':                        
                            this.sprite.body.position.x+=this.velocity;
                            this.sprite.scale.x=this.size;
                            this.playAnimation('runSide');
                            break;
                        case'u':
                            this.sprite.body.position.y-=this.velocity;
                            this.sprite.scale.x=this.size;
                            this.playAnimation('runUp');
                            break;
                        case'd':
                            this.sprite.body.position.y+=this.velocity;
                            this.sprite.scale.x=this.size;
                            this.playAnimation('runDown');
                            break;                     
                    }
                }
            }
             else{
                 
                if(this.pad.left.isDown)
                    this.sprite.body.velocity.x-=this.velocity;
                if(this.pad.right.isDown)
                    this.sprite.body.velocity.x+=this.velocity;
                if(this.pad.up.isDown)
                    this.sprite.body.velocity.y-=this.velocity;
                if(this.pad.down.isDown)
                    this.sprite.body.velocity.y+=this.velocity;
            }
            
       }
        else{
            this.sprite.body.velocity.x=0;
            this.sprite.body.velocity.y=0;
        }
        
        //collision
        if(this.canCollide){
            for(var i=0; i<this.collidingList.length; ++i){
                //digdug.game.physics.arcade.collide(this.sprite, this.collidingList[i]);
            }
        }
        
        this.posX=this.sprite.body.position.x;
        this.posY=this.sprite.body.position.y;   
        
        //deathCheck 
        if(this.canBeHarmed){
            this.timer=digdug.game.time.now;
        }
        else{
            if(digdug.game.time.now>this.timer+1000){            
                this.canBeHarmed=true;
                this.resetLevel=true;    
            }
        }
        
        
        //ataque
        if(this.arrow==1 && this.heroArrow.enemyLinked!=0){
            if((this.pad.down.isDown || this.pad.up.isDown || this.pad.right.isDown || this.pad.left.isDown) || 
              (this.heroArrow.enemyLinked.inflation<=0)){
                this.hearInput=true;
                this.arrow=0;  
                this.heroArrow.sprite.destroy();
            }
            
            if(this.attackButtonNewState!=this.attackButtonLastState && this.attackButtonNewState){
                this.heroArrow.inflateSignal=true;
                if(this.heroArrow.enemyLinked.inflation > 45){
                    this.hearInput=true;
                    this.arrow=0;  
                    this.heroArrow.sprite.destroy();
                }
            }
            if(this.heroArrow.spaceCounter>3){
                this.hearInput=true;
                this.arrow=0;  
                this.heroArrow.sprite.destroy();
            }
        }
        else{
            if(!this.attackButtonNewState && this.arrow==0 && this.frontVec!='n'){
                this.attack();
                this.arrow=1;
                this.hearInput=false;
                this.stopAnimation('runSide');
                this.stopAnimation('runUp');
                this.stopAnimation('runDown');
                switch(this.frontVec){
                    case 'r':this.sprite.frame=6;
                            this.sprite.scale.setTo(this.size, this.size);
                        break;
                    case 'l':this.sprite.frame=6;
                            this.sprite.scale.setTo(-this.size, this.size);
                        break;
                    case 'u':this.sprite.frame=13;
                            this.sprite.scale.setTo(this.size, this.size);
                        break;
                    case 'd':this.sprite.frame=20;
                            this.sprite.scale.setTo(this.size, this.size);
                        break;
                }
            }
            else if(this.attackButtonNewState && this.arrow == 1){         
                this.heroArrow.sprite.destroy();
                this.hearInput=true;
                switch(this.frontVec){
                    case 'r':this.sprite.frame=0;
                            this.sprite.scale.setTo(this.size, this.size);
                        break;
                    case 'l':this.sprite.frame=0;
                            this.sprite.scale.setTo(-this.size, this.size);
                        break;
                    case 'u':this.sprite.frame=7;
                            this.sprite.scale.setTo(this.size, this.size);
                        break;
                    case 'd':this.sprite.frame=14;
                            this.sprite.scale.setTo(this.size, this.size);
                        break;
                }
                this.arrow=0;
            }
        }
        
        if(this.arrow==1){
            this.heroArrow.update();
            switch(this.frontVec){
                case 'r':
                    if(this.heroArrow.sprite.body.position.x>(this.sprite.body.position.x+120)){
                        this.heroArrow.sprite.destroy();
                        delete this.heroArrow;
                        this.arrow =0;
                    }
                    break;
                case 'l':
                    if(this.heroArrow.sprite.body.position.x<(this.sprite.body.position.x-90)){
                        this.heroArrow.sprite.destroy();
                        delete this.heroArrow;
                        this.arrow =0;
                    }
                    break;
                case 'u':
                    if(this.heroArrow.sprite.body.position.y<(this.sprite.body.position.y-90)){
                        this.heroArrow.sprite.destroy();
                        delete this.heroArrow;
                        this.arrow =0;
                    }
                    break;
                case 'd':
                    if(this.heroArrow.sprite.body.position.y>(this.sprite.body.position.y+90)){
                        this.heroArrow.sprite.destroy();
                        delete this.heroArrow;
                        this.arrow =0;
                    }
                    break;
            }
        }
            
        
        //utilities
        this.sprite.bringToTop();
        this.attackButtonLastState=this.attackButtonNewState;
    },    
        
    //instance method
    make:function(id, config){
        var newObject = Object.create(this);
        
        this.id=id;
        
        if (config !== undefined) {
            Object.keys(config).forEach(function(key) {
            newObject[key] = config[key];
            });
        }
        return newObject;
    },
    
    //custom methods
    setPos:function(newX, newY){
      this.sprite.body.position.x=newX;  
      this.sprite.body.position.y=newY;  
    },
    setSize:function(newSize){
        this.size=newSize;
        this.sprite.scale.setTo(this.size);  
    },
    enablePhysics:function(juego, boolVal){
        juego.physics.arcade.enable(this.sprite);        
        this.sprite.body.collideWorldBounds=boolVal;
    },   
    setGravity:function(newGrav){
        if(newGrav==0)
            this.sprite.body.allowGravity = false;
        else
            this.sprite.body.allowGravity = true;
        this.sprite.body.gravity.y=newGrav;
    },
    addToCollidingList:function(newColider){
        this.collidingList.push(newColider);
    },
    setColor:function(newColor){
        this.color=newColor;        
    },
    setVelocity:function(newVel){
        this.velocity=newVel;   
    },
    setVelMode:function(velocityMode){
        this.velMode=velocityMode;
    },
    addAnimation:function(animName, animFrames, frameRate, loop){
        this.sprite.animations.add(animName,animFrames, frameRate, loop);
    },
    playAnimation:function(animName){
        if(!this.animLock)
            this.sprite.animations.play(animName);
    },
    stopAnimation:function(animName){
        this.sprite.animations.stop(animName);
    },
    spawnHole:function(game){
        game.add.image(this.sprite.body.position.x, this.sprite.body.position.y, 'hole');
    },
    attack:function(game){
        if(this.frontVec!='n'){
            this.arrow=1;
            this.heroArrow = digdug.heroArrow.make('arrow');
            this.heroArrow.size=2;
            this.heroArrow.preload();
            var localOffset=this.sprite.height/2;
            var localOffset2=this.sprite.width/2;
            var arrowSpeed=2;
            switch(this.frontVec){
                case 'l':
                    this.heroArrow.create(this.sprite.left+this.sprite.width/2, this.sprite.position.y, 'arrowRight', 2, 'l');
                    this.heroArrow.velocityX=-arrowSpeed;
                    this.heroArrow.velocityY=0;
                    this.heroArrow.sprite.scale.setTo(-this.heroArrow.size, this.heroArrow.size);
                    break;
                case 'r':
                    this.heroArrow.create(this.sprite.right-this.sprite.width/2, this.sprite.position.y, 'arrowRight', 2, 'r');
                    this.heroArrow.velocityX=arrowSpeed;
                    this.heroArrow.velocityY=0;
                    //this.heroArrow.scale.setTo(-1, 1);
                    break;
                case 'u':
                    this.heroArrow.create(this.posX+localOffset, this.posY+localOffset2, 'arrowRight', 2, 'u');
                    this.heroArrow.velocityX=0;
                    this.heroArrow.velocityY=-arrowSpeed;
                    this.heroArrow.sprite.angle=90;
                    this.heroArrow.sprite.scale.setTo(-this.heroArrow.size, this.heroArrow.size);
                    break;
                case 'd':
                    this.heroArrow.create(this.posX+localOffset, this.posY+localOffset2, 'arrowRight', 2, 'd');
                    this.heroArrow.velocityX=0;
                    this.heroArrow.velocityY=arrowSpeed;
                    this.heroArrow.sprite.angle=90;
                    this.heroArrow.sprite.scale.setTo(this.heroArrow.size, this.heroArrow.size);
                    break;
                case 'n':
                    break;
            }
            //explosive refs for arrow
            for(var i=0; i<this.explosiveList.length; ++i){
                this.heroArrow.explosiveList.push(this.explosiveList[i]);
            }
            //enemies refs for arrow
            for(var i=0; i<this.enemies.length; ++i){
                this.heroArrow.enemies.push(this.enemies[i]);
            }
        }
    },
    setEnemyList(enemyList){
        for(var i=0; i<enemyList.length; ++i){
            this.enemies.push(enemyList[i]);
        }
    },
    kill:function(){
        if(this.canBeHarmed && !this.resetLevel){
            this.stopAnimation();
            this.animLock=false;
            this.playAnimation('death');
            this.canBeHarmed=false;
            this.hearInput=false;
            this.canCollide=false;
            this.isAlive = false;
            this.walkMusic.stop();
        }
    }
};