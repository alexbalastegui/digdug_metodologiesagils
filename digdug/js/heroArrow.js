var digdug = digdug || {};

digdug.heroArrow = {
    id:"",
    size: 1,
    posX:0,
    posY:0,
    color: 0xffffff,
    velocityX:1,
    velocityY:0,
    collidingList:[],
    enemies:[],
    explosiveList:[],
    enemyLinked:0,
    inflateSignal:false,
    spaceCounter:0,
    
    //phaser methods
    preload:function(){
    },
    create:function(spawnX, spawnY, spriteName, frame, dir){
        this.posX=spawnX,
        this.posY=spawnY,
        this.sprite=digdug.game.add.sprite(this.posX,this.posY,spriteName,frame);
        this.sprite.anchor.setTo(1, .5);
        this.sprite.scale.setTo(this.size);
        this.sprite.tint = this.color;
        switch (dir){
            case 'u':
                this.mask=digdug.game.add.graphics(0, 0);
                this.mask.beginFill(0x000000); 
                this.mask.drawRect(0, 0, gameVars.gameWidth, spawnY);
                break;
            case 'd':
                this.mask=digdug.game.add.graphics(0, 0);
                this.mask.beginFill(0x000000); 
                this.mask.drawRect(0, spawnY, gameVars.gameWidth, gameVars.gameHeight/2);
                break;
            case 'r':
                this.mask=digdug.game.add.graphics(0, 0);
                this.mask.beginFill(0x000000); 
                this.mask.drawRect(spawnX, 0, gameVars.gameWidth-spawnX, gameVars.gameHeight);
                break;
            case 'l':
                this.mask=digdug.game.add.graphics(0, 0);
                this.mask.beginFill(0x000000); 
                this.mask.drawRect(0, 0, spawnX, gameVars.gameHeight);
                this.deathDist=spawnX-this.sprite.width;
                break;
        }
        this.sprite.mask=this.mask;
        this.mask=null;
        this.sprite.mask.alpha=0;
        
        digdug.game.physics.enable(this.sprite);
        this.sprite.body.allowGravity = false;
        
        this.sprite.body.setSize(10, 10, 50, 0);        
        
    },
    update:function(){  
        //movement
        this.sprite.body.position.x+=this.velocityX;
        this.sprite.body.position.y+=this.velocityY;
        this.posX=this.sprite.body.position.x;
        this.posY=this.sprite.body.position.y;        
        
        
        //collision
        for(var i=0; i<this.enemies.length; ++i){
            digdug.game.physics.arcade.overlap(this.sprite, this.enemies[i], this.harm, null, this);
        }
        
        //overlap
        for(var i=0; i<this.explosiveList.length; ++i){
            digdug.game.physics.arcade.overlap(this.sprite, this.explosiveList[i].sprite, this.explode, null, this);
        }
        
        if(this.enemyLinked!=0){
            if(this.inflateSignal){
                if(this.spaceCounter<3){
                this.enemyLinked.InflateMe();
                this.inflateSignal=false;
                this.spaceCounter++;
                }
                else{
                    this.spaceCounter=0;
                    this.inflateSignal=false;
                    this.enemyLinked=0;
                }
            }
        }
        
        
        this.sprite.bringToTop();
    },    
        
    //instance method
    make:function(id, config){
        var newObject = Object.create(this);
        
        this.id=id;
        
        if (config !== undefined) {
            Object.keys(config).forEach(function(key) {
            newObject[key] = config[key];
            });
        }
        return newObject;
    },
    
    //custom methods
    harm:function(arrow, enemy){
        this.velocityX=0;
        this.velocityY=0;
        //enemy.isAlive=false;
        //enemy.kill();
        enemy.stopMe();
        digdug.baseCharacter.punt+=300;
        if(this.enemyLinked==0)
            enemy.inflation=5;
        this.enemyLinked=enemy;
        //necesito una manera de hacer que el enemigo se detenga y poder cambiarle los sprites por lo de estar inflandose
    },
    explode:function(arrow, explosive){
        this.velocityX=0;
        this.velocityY=0;
        explosive.animations.play('explosion');
        explosive.isExploding=true;
        explosive.hitInstant=digdug.game.time.now;
    }
    
};