var digdug = digdug || {};

digdug.explosive = {
    id:"",
    size: 1,
    posX:0,
    posY:0,
    timeToExplode:0,
    burstNow:false,
    
    //phaser methods
    preload:function(){
    },
    create:function(spawnX, spawnY){        
        this.sprite=digdug.game.add.sprite(spawnX,spawnY,'explosive', 0);
        this.sprite.isExploding=false;
        this.sprite.hitInstant=0;
        this.timeToExplode=2000;
    },
    update:function(){
        if(this.sprite.isExploding)
        {
            if((digdug.game.time.now - this.sprite.hitInstant)>2000 && this.sprite.hitInstant!=0){
                this.sprite.animations.stop();
                this.sprite.frame=3;
                if((digdug.game.time.now - this.sprite.hitInstant)>2200)
                    this.burstNow=true;
            }
            
        }
        //console.log(this.sprite.hitInstant);
    },    
    
    //instance method
    make:function(id, config){
        var newObject = Object.create(this);
        
        this.id=id;
        
        if (config !== undefined) {
            Object.keys(config).forEach(function(key) {
            newObject[key] = config[key];
            });
        }
        return newObject;
    }
    
    //custom methods
};