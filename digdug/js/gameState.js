var digdug = digdug || {};


digdug.gameState = {
    preload:function(){
        //físicas
        digdug.game.physics.startSystem(Phaser.Physics.ARCADE);
        digdug.game.physics.arcade.gravity.y=gameVars.gravity;
        
        this.stage.backgroundColor="#00F000";
        
        this.load.spritesheet('hero','assets/img/spriteSheetHero.png',32,32);
        
        this.hero=digdug.baseCharacter.make('circle');
        this.hero.setVelMode(false);
                
        this.hero.preload();      
    },
    create:function(){
        this.hero.create(200, 50, 'hero');   
        //this.hero.size=(5);
        this.hero.enablePhysics(digdug.game, false);
        this.hero.setGravity(0);
        
        this.hero.addAnimation('test', [0, 1, 2, 3], 3, true);
        this.hero.playAnimation('test');
        
    },
    
    loadHoles:function(){
        this.holes = this.add.group();
        this.holes.enableBody=true;
        
        //this.enemiesTimer = this.game.time.events.loop(Phaser.Timer.SECOND*2,this.createEnemy,this);
    },
    
    createHole:function(){
        var hole = this.holes.getFirstExists(false);
        var posX = this.hero.body.position.x;
        var posY = this.hero.body.position.y;
        if(!hole){    
            hole = new Hole(this.game,posX,posY);
            this.holes.add(hole);
        }else{
            hole.reset(posX,posY);
        }
        if (posX<enemy.width/2){
                enemy.x=enemy.width/2;
        }else if(posX>this.game.width-enemy.width/2){
            enemy.x=this.game.width-enemy.width/2;
        }
        //darle velocidad
        enemy.body.velocity.y =this.enemySpeed;
    },
    
    update:function(){
        this.hero.update();
    }
};