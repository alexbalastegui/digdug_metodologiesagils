var digdug = digdug || {};

digdug.creditsscene={
    preload:function(){
       this.load.spritesheet('dragon','assets/img/dragon.png',36 ,36);
    },
    create:function(){
      this.title=this.game.add.text(gameVars.gameWidth/2,this.game.world.centerY/2,'CREDITS', {font: '50px arcade',fill: '#ff0000'});
      this.title.anchor.setTo(.5);
      this.line=this.game.add.text(gameVars.gameWidth/2,this.game.world.centerY/2+20,'--------', {font: '50px arcade',fill: '#00ff00'});
      this.line.anchor.setTo(.5);
      this.Miquel=this.game.add.text(gameVars.gameWidth/2-150,this.game.world.centerY/2+50,'Miquel Barnada', {font: '50px arcade',fill: '#00ff00'});
      this.line.anchor.setTo(.5);
      this.Alex=this.game.add.text(gameVars.gameWidth/2-150,this.game.world.centerY/2+90,'Alex Balastegui', {font: '50px arcade',fill: '#00ff00'});
      this.line.anchor.setTo(.5);
      this.Oscar= this.game.add.text(gameVars.gameWidth/2-150,this.game.world.centerY/2+130,'Oscar Espejo', {font: '50px arcade',fill: '#00ff00'});
      this.line.anchor.setTo(.5);
        
      this.dragon = this.game.add.sprite(gameVars.gameWidth/2,this.game.world.centerY/2+200,'dragon');
      this.dragon.animations.add('walk',[0,1],6,true);
      this.dragon.anchor.setTo(.5);
      this.dragon.scale.setTo(1.25);
    },
    update:function(){
        this.dragon.play('walk');
    }
}