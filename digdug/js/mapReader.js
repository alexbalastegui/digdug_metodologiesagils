
var digdug = digdug || {};

var num = 1; //valor que representa el nivel donde se encuentra el player
var lives = 3;
var score = 0;
var ranking;


var localStorageName = "RANKING";


digdug.loadlevel = {
    init:function(){
        this.MapSelector(); //funcion que selecciona el nivel en funcion de en que nivel esta el player
        this.pauseButton=digdug.game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.game.isPaused=false;
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.world.enableBody = true;
        this.holeList=[];
        this.enemylist = [];
        this.explosionCol=0;
        //this.Nodes = new HashMap();
        this.rankinginstorage = JSON.parse(localStorage.getItem(localStorageName));
        this.rankinginstorage.sort(function(a,b){return b-a});
    },
    resetLives:function(){
        lives=3;
    },
    preload:function(){
        //character
        this.load.spritesheet('hero','assets/img/hero.png',16,16);
        this.hero = digdug.baseCharacter.make('hero');
        this.hero.setVelMode(false);
        this.hero.preload();  
        this.hero.canBeHarmed=true;
        
        this.game.load.spritesheet('explosive','assets/img/explosive.png', 36, 36);
        this.game.load.spritesheet('verduras','assets/img/verduras.png', 16, 16);
        
        //level
        this.load.image('ground','assets/img/background.png');
        this.load.image('pixel','assets/img/pixel.png');
        this.load.image('sky', 'assets/img/sky.png');
        this.load.spritesheet('dragon','assets/img/dragon.png',36 ,36);
        this.load.spritesheet('gafas','assets/img/gafotas.png',36 ,36);
        this.load.image('barrel', 'assets/img/barril.png');
        // se carga en el player this.load.image('hole', 'assets/img/Hole.png');
        this.load.spritesheet('rock', 'assets/img/rock.png', 36,36);
        this.load.spritesheet('wflower', 'assets/img/wflower.png', 56,13);
        this.load.image('flower1', 'assets/img/flower01.png');
        this.load.image('flower2', 'assets/img/flower02.png');
        this.load.spritesheet('fire', 'assets/img/fire.png', 108,36);
        
        //sounds
        this.load.audio('walkAudio', 'assets/audio/Walking.mp3');
        this.load.audio('gameOverAudio', 'assets/audio/DidDugDisappearing.mp3');
        this.load.audio('firingAudio', 'assets/audio/Firing.mp3');
        this.load.audio('harpoonAudio', 'assets/audio/Harpoon.mp3');
        this.load.audio('m_BlownAudio', 'assets/audio/MonsterBlownOut.mp3');
        this.load.audio('m_HitDAudio', 'assets/audio/MonsterHitDigDug.mp3');
        this.load.audio('m_SquashAudio', 'assets/audio/MonsterSquashed.mp3');
        this.load.audio('pumpAudio', 'assets/audio/Pumping.mp3');
        this.load.audio('rockFallAudio', 'assets/audio/RockFall.mp3');
        this.load.audio('rockFLoorAudio', 'assets/audio/RockFloor.mp3');
        
        
        this.load.image('invisibleCol', 'assets/img/invisible.png'); //CHANGE THE IMAGE <-------------------------------------------------------
        this.load.spritesheet('hud', 'assets/img/lifehud.png',42,14);
        
        //menu pause assets
        this.load.image('pause', 'assets/img/pause.png');
        this.load.image('continue', 'assets/img/continue.png');
        this.load.image('ranking', 'assets/img/ranking.png');
    },
    MapSelector:function(){ //funcion que dependiendo el nivel en el que estemos carga un nivel o otro
        switch(num){
            case 1:
            this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '1101110111111',
                '1101R10111111',
                '11G1110111111',
                '110111011B111',
                '1101110111111',
                '1111110111R11',
                '1111110111111',
                '10N0100011111',
                '1111111111111',
                '11R11B1111111',
                '1111111111011',
                '1111111111V11',
                '110G01B111011',
                '1111111111111',
                '1111111111111'
            ];
            break;
            case 2:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '110111V111011',
                '1101110111N11',
                '11G1110111011',
                '1101110111111',
                '110111011R111',
                '1111110111111',
                '111R110111111',
                '1111100011111',//6-8
                '1111110111R11',
                '1111110111111',
                '1111110111011',
                '1111111111011',
                '110N011111G11',
                '1111111111011',
                '1111111111111'
            ];
            break;
            case 3:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '1111110111011',
                '1111110110N01',
                '1111110111011',
                '1101110111011',
                '1101110111111',
                '1101110111R11',
                '10G0110111111',
                '1111100011111',
                '111111B111111',
                '11R1111111111',
                '110V011111011',
                '1111111111N11',
                '110G01B111011',
                '1111111111111',
                '1111111111111'
            ];
            break;
            case 4:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '10V0110111011',
                '1111110110N01',
                '1000110111011',
                '1101110110N01',
                '1000110111111',
                '1101110111R11',
                '10N0110111111',
                '1111100011011',
                '111111B111011',
                '11R1111110N01',
                '111B111111011',
                '1111111111N11',
                '110N01B111011',
                '1111111111111',
                '1111111111111'
            ];
            break;
            case 5:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '1111110111011',
                '1111110111N11',
                '1101110111011',
                '1101110111G11',
                '1101110111111',
                '1101110111R11',
                '11N1110111111',
                '1111100011011',
                '111111B111011',
                '11R1111111N11',
                '111B111111011',
                '1111111111N11',
                '110V01B111011',
                '1111111111111',
                '1111111111111'
            ];
            break;
            case 6:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '1111110111111',
                '1111110110N01',
                '10G0110111111',
                '1111110110N01',
                '1111111011111',
                '1111111011R11',
                '10V0110111111',
                '1111100011011',
                '111111B111011',
                '11R1111111G11',
                '101B111111011',
                '1G11111111G11',
                '101111B111011',
                '1111111111111',
                '1111111111111'
            ];
            break;
            case 7:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '1000110111111',
                '1111110110001',
                '10G0110111111',
                '1111110110N01',
                '1000110111111',
                '1111110111R11',
                '10V0110111111',
                '1111100011011',
                '111110B011011',
                '11R1100011G11',
                '101B110111011',
                '1011000001G11',
                '101101B101011',
                '1111111111111',
                '1111111111111'
            ];
            break;
            case 8:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '10V0110111111',
                '1111110110001',
                '10N0110111111',
                '1111110110N01',
                '1000110111111',
                '1111110111R11',
                '10N0110110101',
                '1111100011011',
                '111100B001011',
                '11R1100011G11',
                '101B110111011',
                '1011000001N11',
                '101101B101011',
                '1111111111111',
                '1111111111111'
            ];
            break;
            case 9:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '1000110111111',
                '1111110110001',
                '10N0110111111',
                '1111110110N01',
                '1000110111111',
                '1111110111R11',
                '10N0110110101',
                '1111100011011',
                '111100B001011',
                '11R1100011G11',
                '101B110111011',
                '1011000001N11',
                '101101B101011',
                '1111111111111',
                '1111111111111'
            ];
            break;
            case 10:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '1000110111111',
                '1111110110001',
                '10N0110111111',
                '1111110110N01',
                '1000110111111',
                '1111110111R11',
                '10N0110110101',
                '1111100011011',
                '111100B001011',
                '11R1100011G11',
                '101B110111011',
                '1011000001N11',
                '101101B101011',
                '1111111111111',
                '1111111111111'
            ];
            break;
            case 11:
                this.level=[ //sin tener en cuenta el cielo
                '1111110111111',
                '1111110111111',
                '1111110111111',
                '1111110111111',
                '1111110110N01',
                '1111110111111',
                '1111110111R11',
                '10N0110110101',
                '1111100011011',
                '111100B001011',
                '11R1100011G11',
                '101B110111011',
                '1011000001N11',
                '101101B101011',
                '1111111111111',
                '1111111111111'
            ];
            break;
        }
    },
    
    FlowerLevel:function(){
        this.wflower=this.game.add.sprite(365,88,'wflower');
        this.wflower.scale.setTo(1.8);
        this.wflower.animations.add('level',[0,1,2,3],10,true);
        switch(num){
            case 1:
               this.wflower.frame = 0;
            break;
            case 2:
               this.wflower.frame = 1;
            break;
            case 3:
               this.wflower.frame = 2;
            break;
            case 4:
               this.wflower.frame = 3;
            break;
            case 5:
               this.rflower = this.game.add.image(440,70,'flower1');
               this.rflower.scale.setTo(1.8);
               this.wflower.destroy();
            break;
            case 6:
                this.rflower = this.game.add.image(440,70,'flower1');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.frame = 0;
            break;
            case 7:
                this.rflower = this.game.add.image(440,70,'flower1');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.frame = 1;
            break;
            case 8:
                this.rflower = this.game.add.image(440,70,'flower1');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.frame = 2;
            break;
            case 9:
                this.rflower = this.game.add.image(440,70,'flower1');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.frame = 3;
            break;
            case 10:
                this.rflower = this.game.add.image(440,70,'flower2');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.destroy();
            break;
            case 11:
                this.rflower = this.game.add.image(440,70,'flower2');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.frame = 0;
            break;
            case 12:
                this.rflower = this.game.add.image(440,70,'flower2');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.frame = 1;
            break;
            case 13:
                this.rflower = this.game.add.image(440,70,'flower2');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.frame = 2;
            break;
            case 14:
                this.rflower = this.game.add.image(440,70,'flower2');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.frame = 3;
            break;
            case 15:
                this.rflower = this.game.add.image(440,70,'flower2');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 30;
                this.wflower.destroy();
                this.rflower = this.game.add.image(440,70,'flower2');
                this.rflower.scale.setTo(1.8);
                this.wflower.x -= 60;
                
            break;
        }
    },

    LifeHud:function(){
        this.hud=this.game.add.sprite(10,gameVars.gameHeight-50,'hud');
        this.hud.scale.setTo(3);
        this.hud.animations.add('level',[0,1,2,3],10,true);
        switch(lives){
            case 0:
                this.hud.frame = 0;
                break;
            case 1:
                this.hud.frame = 1;
                break;
            case 2:
                this.hud.frame = 2;
                break;
            case 3:
                this.hud.frame = 3;
                break;
        }
    },
    
    create:function(){
        //max points
        //levelSetUp
        this.sky = this.add.image(0,0, 'sky') //añadimos el fondo del cielo
        this.skyCollider=digdug.game.add.sprite(0, 0, 'pixel');
        this.skyCollider.scale.x=gameVars.gameWidth;
        this.skyCollider.scale.y=105;
        
        digdug.game.physics.arcade.enable(this.skyCollider);   
        this.skyCollider.enableBody=true;
        this.skyCollider.body.immovable=true;
        
        this.skyCollider.tint=0x000096;
        
        this.bottomCollider=digdug.game.add.sprite(0, gameVars.gameHeight-58, 'pixel');
        this.bottomCollider.scale.x=gameVars.gameWidth;
        this.bottomCollider.scale.y=80;
        
        digdug.game.physics.arcade.enable(this.bottomCollider);   
        this.bottomCollider.enableBody=true;
        this.bottomCollider.body.immovable=true;
        
        this.bottomCollider.tint=0x000000;
        
        this.LifeHud();
        this.FlowerLevel();
        this.bg = this.add.image(0,109,'ground'); //ponemos el ground a partir del nivel de la pantalla que queremos
        this.bg.scale.setTo(2.2);
        
        this.holes = this.game.add.group();
        this.rocks = [];
        this.explosiveList = [];
        this.verduraList = [];
        
        this.timetostalk = 6500;
        this.id = 0;
        this.Path = [];
        this.grid =   [[0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0], //25x33 (fila superior, cel)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //0 (2)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //1 (4)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //2 (6)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //3 (8)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //4 (10)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //5 (12)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //6 (14)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //7 (16)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //8 (18)
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1],
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1],
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1],
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1],
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1],
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1],
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1], //fila intermiga
                      [1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1]
                     ];
        
        //console.log("---- "+this.level.length+" ----- "+this.level[0].length); //16 files x 13 columnes
        for (var i=0; i<this.level.length;i++){ //for que recorre toda la varieble level y genera el nivel en funcion de lo que encuentra
            for (var j=0; j<this.level[i].length;j++){
                switch(this.level[i][j]){
                    case 'V':
                        this.verdura = digdug.verduras.make('verdura');
                        this.verdura.preload();
                        this.verdura.create(gameVars.gameCell*j,gameVars.gameCell*i+108, this.hero);
                        digdug.game.physics.arcade.enable(this.verdura.sprite);
                        this.verdura.sprite.enableBody=true;
                        this.verdura.sprite.body.immovable=true;
                        
                        this.verduraList.push(this.verdura); 
                        //sin break para que cree agujero
                    case '0':
                    case 'N':
                    case 'G':
                        //añado el mas +108 para que empiece a partir del suelo
                        //ADD HOLE & CHECK CONECTED HOLES
                        this.grid[(i*2)+2][j*2] = 0;

                        var hole = this.game.add.sprite(gameVars.gameCell*j,gameVars.gameCell*i+108,'hole'); 
                        hole.anchor.setTo(.0);
                        hole.body.immovable = true;
                        //hole.body.setSize(20, 20, 15, 15);
                        this.holes.add(hole);
                        
                        if (i>0 && (this.level[i-1][j] == 0 || this.level[i-1][j] == 'N' || this.level[i-1][j] == 'G' || this.level[i-1][j] == 'V')){ //te forat a sobre
                            
                            this.grid[(i*2)+1][j*2] = 0;
                            
                            hole = this.game.add.sprite(gameVars.gameCell*j,gameVars.gameCell*i+108-gameVars.gameCell/2,'hole'); 
                            hole.anchor.setTo(.0);
                            hole.body.immovable = true;
                            //hole.body.setSize(20, 20, 15, 15);
                            this.holes.add(hole);
                        }
                        if (j>0 && (this.level[i][j-1] == 0 || this.level[i][j-1] == 'N' || this.level[i][j-1] == 'G' || this.level[i][j-1] == 'V')){ // forat a la esq
                            
                            this.grid[(i*2)+2][(j*2)-1] = 0;
                            
                            hole = this.game.add.sprite(gameVars.gameCell*j-gameVars.gameCell/2,gameVars.gameCell*i+108,'hole'); 
                            hole.anchor.setTo(.0);
                            hole.body.immovable = true;
                            //hole.body.setSize(20, 20, 15, 15);
                            this.holes.add(hole);
                            
                        }else if (i == 0){
                            this.grid[1][j*2] = 0;
                        }
                        break;
                    case 'R':
                        this.rock = new digdug.rockPrefab(this.game,gameVars.gameCell*j+gameVars.gameCell/2,gameVars.gameCell*i+108+gameVars.gameCell/2,this);
                        this.game.add.existing(this.rock);
                        this.rocks.push(this.rock);
                        break;
                    case 'B':
                        this.explosive = digdug.explosive.make('explosive');
                        this.explosive.preload();
                        this.explosive.create(gameVars.gameCell*j,gameVars.gameCell*i+108);
                        digdug.game.physics.arcade.enable(this.explosive.sprite);
                        this.explosive.sprite.enableBody=true;
                        this.explosive.sprite.body.immovable=true;
                        this.explosive.sprite.animations.add('explosion',[1, 0], 5, true);
                        //this.explosive.sprite.animations.play('explosion');
                        
                        this.explosiveList.push(this.explosive);
                        break;
                }
            }
        }
        //enemy setup
        for (i=0; i<this.level.length;i++){
            for (j=0; j<this.level[i].length;j++){
                if(this.level[i][j] == 'N'){
                    this.dragon = new digdug.enemy(this.game, 36*j+36/2, 36*i+108+36/2, 30, 1, this, this.timetostalk, this.id,this.Path, 'dragon');
                    this.timetostalk += 4640;
                    this.id += 1;
                    this.game.add.existing(this.dragon);
                    this.enemylist.push(this.dragon);
                }
               
                if(this.level[i][j] == 'G'){
                    this.gafas = new digdug.enemy(this.game, 36*j+36/2, 36*i+108+36/2, 30, 1, this, this.timetostalk, this.id,this.Path, 'gafas');
                    this.timetostalk += 4640;
                    this.id += 1;
                    this.game.add.existing(this.gafas);
                    this.enemylist.push(this.gafas);
                }
            }
        }
        
        //explosive heroRefs
        for(var z=0; z<this.explosiveList.length; ++z){
            this.hero.explosiveList.push(this.explosiveList[z]);
        }

        //print map
        //console.log(this.level);
        //console.log(this.grid);
        
        //character
        this.hero.create(gameVars.gameCell*6+gameVars.gameCell/2, gameVars.gameCell*11+gameVars.gameCell/2, 'hero');   
        this.hero.setSize(2);
        this.hero.enablePhysics(digdug.game, true);
        this.hero.setGravity(0);
        this.hero.addToCollidingList(this.skyCollider);
        this.hero.addToCollidingList(this.bottomCollider);
        this.hero.setEnemyList(this.enemylist);
        
        this.hero.addAnimation('runSide', [0, 1], 10, true);
        this.hero.addAnimation('runUp', [7, 8], 10, true);
        this.hero.addAnimation('runDown', [14, 15], 10, true);
        this.hero.addAnimation('death', [21, 22, 23, 24, 25], 6, true);
        
        this.ready=this.game.add.text(gameVars.gameWidth/2-50,this.game.world.centerY+100, 'READY',{font: '30px arcade',fill: '#fff'});
        
        this.player1message=this.game.add.text(gameVars.gameWidth/2-60,this.game.world.centerY, 'PLAYER 1',{font: '30px arcade',fill: '#fff'});
        
        this.highscore=this.game.add.text(gameVars.gameWidth/2-50,this.game.world.centerY-340,'HIGH SCORE',{font: '30px arcade',fill: '#ff0000'});
        
        this.life=this.game.add.text(gameVars.gameWidth/2-180,this.game.world.centerY-340,'1 up', {font: '30px arcade',fill: '#ff0000'});   
        
        this.currentscore=this.game.add.text(gameVars.gameWidth/2-170,this.game.world.centerY-315, this.hero.sprite.punt,{font: '30px arcade',fill: '#fff'});

        
        this.highscorenumber=this.game.add.text(gameVars.gameWidth/2-13,this.game.world.centerY-310,this.rankinginstorage[0],{font: '30px arcade',fill: '#fff'});
        
        this.rounds=this.game.add.text(gameVars.gameWidth/2+120,this.game.world.centerY+280,'ROUND',{font: '30px arcade',fill: '#fff'});
        
        this.numberofrounds=this.game.add.text(gameVars.gameWidth/2+200,this.game.world.centerY+300,num,{font: '30px arcade',fill: '#fff'});
        

        this.counter = 0;
        this.id = 0;
        
        this.startTime=this.time.now;
        
    },
    heroKill:function(explosionCol, heroSpr){
        this.hero.kill();
        console.log("HERO KILL is being called");
    },
    enemyKill:function(enemySpr, explosionCol){
        enemySpr.killMe();
        console.log("ENEMY KILL is being called");
    },
    update:function(){
        this.counter ++;
        if (this.counter >= 12){
            this.id = (this.id + 1) % this.enemylist.length;
            this.counter = 0;
        }
        
        if(this.startTime>this.time.now-1700){
            this.ready.setText('READY');
            this.player1message.setText('PLAYER 1');
            this.game.isPaused = true;
        }else{
            this.ready.destroy();
            this.player1message.destroy();
            this.game.isPaused = false;
        }
        this.currentscore.setText(this.hero.sprite.punt);
        
        if(!this.game.isPaused){
            //cuando el juego no está pausado                  
            this.hero.update();
            
            digdug.game.physics.arcade.collide(this.skyCollider, this.hero.sprite, null, null, this);
            digdug.game.physics.arcade.collide(this.bottomCollider, this.hero.sprite, null, null, this);
                         
            //explosives handle
            for (var w=0; w<this.explosiveList.length; ++w){
                this.explosiveList[w].update();
                this.game.physics.arcade.collide(this.hero.sprite, this.explosiveList[w].sprite);   
                
                if(this.explosiveList[w].burstNow){                    
                    for(var y=-2; y<3; ++y){
                        for(var z=-2; z<3; ++z){
                            this.burstX = this.explosiveList[w].sprite.position.x + gameVars.gameCell*y/2;
                            this.burstY = this.explosiveList[w].sprite.position.y + gameVars.gameCell*z/2;
                            var pX = Math.floor((this.burstX + 36/2) * 25 / 468); //my position in the grid
                            var pY = Math.floor((this.burstY + 36/2 - 72) * 33 / 612); //(-72 to delete unused sky pixels)
                            
                            if(this.grid[pY][pX] != 0){
                                hole = this.game.add.sprite(this.burstX, this.burstY,'hole'); 
                                this.hero.sprite.punt+=10;
                                hole.anchor.setTo(.0);
                                hole.body.immovable = true;
                                //hole.body.setSize(20, 20, 15, 15);
                                this.holes.add(hole);
                                //this.grid[this.burstX][this.burstY] = 0; 
                                this.explosiveList[w].sprite.kill(); 

                                this.grid[pY][pX] = 0;
                            }
                        }
                    }
                    this.explosionCol = digdug.game.add.sprite(0, 0, 'pixel')                    
                    this.explosionCol.scale.x=gameVars.gameCell*3;
                    this.explosionCol.scale.y=gameVars.gameCell*3;
                    this.explosionCol.anchor.setTo(0);
                    this.explosionCol.position.x=this.explosiveList[w].sprite.position.x-gameVars.gameCell;
                    this.explosionCol.position.y=this.explosiveList[w].sprite.position.y-gameVars.gameCell;
                    this.explosionCol.alpha=0;
                    digdug.game.physics.arcade.enable(this.explosionCol);
                    this.explosionCol.enableBody=true;
                    digdug.game.physics.arcade.overlap(this.hero.sprite, this.explosionCol, this.hero.kill, null, this);
                    this.explosiveList[w].sprite.destroy();
                    this.explosiveList.splice(w, 1);
                    this.explosionCol.body.immovable=true;
                    this.explosionCol.canHarm=0;
                }
            }
            if(this.explosionCol!=0){
                if(this.explosionCol.canHarm<5){
                    digdug.game.physics.arcade.overlap(this.hero.sprite, this.explosionCol, this.heroKill, null, this);
                    for(var i=0; i<this.enemylist.length; ++i){
                        digdug.game.physics.arcade.overlap(this.enemylist[i], this.explosionCol, this.enemyKill, null, this);
                    }
                    
                }
                //console.log("EXPLOSION OVERLAP");
                this.explosionCol.canHarm++;
            }
            
            
            //verduras handle
            for(w=0; w<this.verduraList.length; ++w){
                this.verduraList[w].update();
            }
            
            //levelReset
                        
            //resetlevel 
            if(this.hero.resetLevel){
                this.destroyWorld();
                
                console.log("harmable: " + this.hero.canBeHarmed);
                console.log("resetLvl: " + this.hero.resetLevel);
                
                lives--;
                if(lives == 0){
                    //digdug.rankingState.score=this.hero.sprite.punt;
                    digdug.rankingState.score=this.hero.sprite.punt;
                    this.game.state.start('rankingState', this);
                }else{
                    score = this.hero.sprite.punt;
                }
                for(var i=0; i<this.enemylist.length; ++i){
                    
                   // this.enemylist[i].body.kill();
                    if (this.enemylist[i].body != null){
                        this.enemylist[i].body.enable=false;
                    }
                    if (this.enemylist[i].fireDetector != null){
                        if (this.enemylist[i].fireDetector.body != null){
                            this.enemylist[i].fireDetector.body.enable=false;
                        }
                    }
                    if (this.enemylist[i].fire != null){
                        this.enemylist[i].fire.body.enable=false;
                    }
                }
                
                this.init();
                this.preload();
                this.create();
                if(lives != 0){
                    this.hero.sprite.punt = score;
                }
                this.hero.canBeHarmed=true;
                this.hero.resetLevel=false;
                this.hero.hearInput=true;
                console.log("harmable: " + this.hero.canBeHarmed);
                console.log("resetLvl: " + this.hero.resetLevel);
            }     
            
            //passlevel if enemies are dead
            var passLevel=true;
            for(var i=0; i<this.enemylist.length && passLevel==true; ++i){
                 if(this.enemylist[i].isAlive){
                     passLevel=false;
                 }
            }
            
            if(passLevel){
                score = this.hero.sprite.punt;
                this.destroyWorld();
                num++;
                this.init();
                this.preload();
                this.create();
                this.hero.sprite.punt = score + 100;
            }
            
            
        }
        
        if(this.pauseButton.isDown && this.pauseButton.downDuration(1) && this.game.paused == false){
            this.mypause();
        }
        else if(this.pauseButton.isDown && this.pauseButton.downDuration(1) && this.game.paused == true){
            this.unpause();
        }
        
        this.playerGridX = Math.floor((this.hero.sprite.body.position.x+36/2) * 25 / 468); //my position in the grid
        this.playerGridY = Math.floor((this.hero.sprite.body.position.y+36/2 - 72) * 33 / 612); //(-72 to delete unused sky pixels)
        
        if (this.grid[this.playerGridY][this.playerGridX] == 1){
            hole = this.game.add.sprite(gameVars.gameCell/2 * this.playerGridX, (gameVars.gameCell/2 * this.playerGridY) + 72,'hole'); 
            this.hero.sprite.punt+=10;
            hole.anchor.setTo(.0);
            hole.body.immovable = true;
            //hole.body.setSize(20, 20, 15, 15);
            this.holes.add(hole);
            this.grid[this.playerGridY][this.playerGridX] = 0;
        }
    },
    unpause:function(){
        if(this.game.paused){
          this.pausetext.destroy();
          this.game.paused = false;  
        }
    },
    mypause:function(){
        this.pausetext = this.add.image(gameVars.gameWidth/2, gameVars.gameHeight/2, 'pause');
        this.pausetext.anchor.setTo(.5);
        this.Pressed = true;
        this.pauseButton.onDown.add(function(){this.unpause();},this);
        this.game.paused = true;
        this.game.input.disable = false;
    },
    destroyWorld:function(){
        for(var i=0; i<this.rocks.length; ++i){
            if (this.rocks[i] != null){
                this.rocks[i].suicide();
            }            
        }  
        for(var i=0; i<this.enemylist.length; ++i){
                    
                   // this.enemylist[i].body.kill();
                    if (this.enemylist[i].body != null){
                        this.enemylist[i].body.enable=false;
                    }
                    if (this.enemylist[i].fireDetector != null){
                        if (this.enemylist[i].fireDetector.body != null){
                            this.enemylist[i].fireDetector.body.enable=false;
                        }
                    }
                    if (this.enemylist[i].fire != null){
                        this.enemylist[i].fire.body.enable=false;
                    }
                }
    }
};



