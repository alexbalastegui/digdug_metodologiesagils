var digdug = digdug || {};


digdug.rockPrefab = function (game, x, y, level) {
    Phaser.Sprite.call(this, game, x, y, 'rock');
    this.frame=0;
    this.animations.add('warning', [0,1],10,true);
    this.animations.add('destroy', [1,2,3],5,false);
    this.anchor.setTo(.5);
    this.level = level;
    
    this.game.physics.arcade.enable(this);
    this.body.allowGravity = false;
    this.body.immovable = true;
    
    this.hit = false;
    this.falling = false;
    
    this.initialY = -10;
    this.prevY = Math.floor((this.body.position.y - 72 + 36/2) * 33 / 612);
    this.prevValue = 1;
    this.enemies = this.game.add.group();
    
    this.fallSound = digdug.game.add.audio('rockFallAudio');
    this.floorSound = digdug.game.add.audio('rockFLoorAudio');
    this.hitSound = digdug.game.add.audio('m_HitDAudio');
};


digdug.rockPrefab.prototype = Object.create(Phaser.Sprite.prototype);
digdug.rockPrefab.prototype.constructor = digdug.rockPrefab; 


digdug.rockPrefab.prototype.update = function(){
    
    this.myGridX = Math.floor((this.body.position.x + 36/2) * 25 / 468); //my position in the grid
    this.myGridY = Math.floor((this.body.position.y - 72 + 36/2) * 33 / 612); //(-72 to delete unused sky pixels)
    var playerGridX = Math.floor((this.level.hero.sprite.body.position.x + 36/2) * 25 / 468); //hero position in the grid
    var playerGridY = Math.floor((this.level.hero.sprite.body.position.y - 72 + 36/2) * 33 / 612); //(-72 to delete unused sky pixels)
    
    //console.log("(X:"+this.myGridX+", Y:"+this.myGridY+")");
    //console.log(this.level.grid);
    
    if (this.level.grid[this.myGridY + 2][this.myGridX] == 0 && !this.falling){
        this.animations.play('warning');
        if (this.initialY < 0){
            this.initialY = this.myGridY;
        }
        if (!(Math.abs(this.myGridX - playerGridX) < 3 && this.myGridY + 2 == playerGridY)){
            this.falling = true;
            this.fallSound.play();
            console.log("test");
        }
    }else if (this.falling 
              && this.level.grid[this.myGridY + 1][this.myGridX] != 0 
              && this.level.grid[this.myGridY + 2][this.myGridX] != 0){
        this.falling = false;
        this.frame = 0;
        this.floorSound.play();
        this.animations.stop()
        
        if (this.initialY > 0 && Math.abs(this.initialY - this.myGridY) > 6){
            this.animations.play('destroy');
            this.hit = true;
            this.body.velocity.y = 0;
            this.animations.currentAnim.onComplete.add(function () {
                this.enemies.forEach(function(enemy){
                    enemy.kill();
                });
                this.level.grid[this.prevY][this.myGridX] = this.prevValue;
                this.level.grid[this.myGridY][this.myGridX] = 0;
                this.destroy();
            }, this);
        }else{
            this.initialY = -10;
        }
    }
    
    this.game.physics.arcade.collide(this, this.level.hero.sprite, this.hitHero, null, this);
    this.game.physics.arcade.collide(this, this.level.enemylist, this.hitEnemy, null, this);
    
    if (!this.hit){
        if (!this.falling){
            this.body.velocity.y = 0;
            
            if (this.prevY != this.myGridY){
                this.level.grid[this.prevY][this.myGridX] = this.prevValue;
                
                this.prevY = this.myGridY;
                this.prevValue = this.level.grid[this.myGridY][this.myGridX];
                this.level.grid[this.myGridY][this.myGridX] = 1;
            }    
            
        }else{
            this.body.velocity.y = 60; //enemies have 40
            this.frame = 0;
            
            if (this.prevY != this.myGridY){
                this.level.grid[this.prevY][this.myGridX] = this.prevValue;
                
                this.prevY = this.myGridY;
                this.prevValue = this.level.grid[this.myGridY][this.myGridX];
                this.level.grid[this.myGridY][this.myGridX] = 1;
            }            
        } 
    }    
           
};

digdug.rockPrefab.prototype.hitHero = function(rock,hero){
    if(rock.body.touching.down && hero.body.touching.up && this.body.velocity.y > 0){
        this.animations.play('destroy');
        this.hit = true;
        this.body.velocity.y = 0;
        this.animations.currentAnim.onComplete.add(function () {
            //player die
            if(!this.hitSound.isPlaying){
                this.hitSound.play();
                this.level.hero.kill();
            }    
            this.level.grid[this.prevY][this.myGridX] = this.prevValue;
            this.level.grid[this.myGridY][this.myGridX] = 0;
            this.destroy()
        }, this);
    }
}

digdug.rockPrefab.prototype.hitEnemy = function(rock, enemy){
    console.log('hitEnemy');
    if(rock.body.touching.down && enemy.body.touching.up && rock.body.velocity.y > 0 && this.falling && enemy.currentState != 1){
        enemy.rocked = true;
        this.enemies.add(enemy);
        enemy.stopMe();
        /*
        this.animations.play('destroy');
        this.hit = true;
        this.body.velocity.y = 0;
        this.animations.currentAnim.onComplete.add(function () {
            //enemy die
            this.level.grid[this.prevY][this.myGridX] = this.prevValue;
            this.level.grid[this.myGridY][this.myGridX] = 0;
            this.destroy()
        }, this);*/
    }
}

digdug.rockPrefab.prototype.suicide = function(){
    if (this.body != null){
        this.body.enable = false;
    }    
    this.destroy();
    
    console.log("I'm sorry father...");
    
}