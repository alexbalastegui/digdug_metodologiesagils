var digdug = digdug || {};

digdug.verduras = {
    id:"",
    size: 1,
    posX:0,
    posY:0,
    
    //phaser methods
    preload:function(){
    },
    create:function(spawnX, spawnY, player){        
        this.sprite=digdug.game.add.sprite(spawnX,spawnY,'verduras', 0);
        this.sprite.scale.setTo(2);
        this.sprite.frame= Math.floor(Math.random()*11);
        this.player=player;
    },
    update:function(){
        digdug.game.physics.arcade.overlap(this.sprite, this.player.sprite, this.pickup, null, this);   
    },    
    
    //instance method
    make:function(id, config){
        var newObject = Object.create(this);
        
        this.id=id;
        
        if (config !== undefined) {
            Object.keys(config).forEach(function(key) {
            newObject[key] = config[key];
            });
        }
        return newObject;
    },
    
    //custom methods
    pickup:function(self, hero){
        this.sprite.kill();
        hero.punt+=100;
    }
};